package hk.test.booster_module

public interface BoosterTracker {
    // Начинает хранение и отслеживание бустера
    fun track(booster: Trackable)
    // Коллекция `id` всех действующих бустеров
    fun activeBoosterIds(): Set<String>

    fun add(listener: BoosterListener)
    fun remove(listener: BoosterListener)
}