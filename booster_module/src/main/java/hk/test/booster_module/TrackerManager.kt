package hk.test.booster_module

import android.content.Context
import hk.test.booster_module.logic.Tracker

class TrackerManager {

    companion object {
        @Volatile private var instance: Tracker? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: Tracker(context).also { instance = it}
        }
    }
}