package hk.test.booster_module

public interface BoosterListener {

    fun onTimeUpdate(trackable: Trackable, timeLeft: Int)
    fun onTimeEnd(trackable: Trackable)
}