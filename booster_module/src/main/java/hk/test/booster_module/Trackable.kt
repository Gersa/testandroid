package hk.test.booster_module

public interface Trackable {
    val productId: String
    val duration: Int?
}