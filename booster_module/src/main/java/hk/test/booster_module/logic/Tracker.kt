package hk.test.booster_module.logic

import android.content.Context
import android.os.Handler
import hk.test.booster_module.BoosterListener
import hk.test.booster_module.BoosterTracker
import hk.test.booster_module.Trackable

class Tracker(context: Context): BoosterTracker {

    private val checkTime = 1 * 1000L
    private val saveTime = 30 * 1000L
    private val trackables: MutableSet<Trackable> = mutableSetOf()
    private val boosterListeners: MutableSet<BoosterListener> = mutableSetOf()
    private val handler: Handler = Handler()
    private val statusMap: MutableMap<String, Int?> = mutableMapOf()
    private val storage = TrackerStorage(context)

    private val trackerRunnable = Runnable {
        checkTime()
    }

    private val saveRunnable = Runnable {
        saveByDelay()
    }

    init {
        statusMap.putAll(storage.get())
        trackables.addAll(storage.loadTrackableById(statusMap.keys))
        handler.post(trackerRunnable)
        handler.postDelayed(saveRunnable, saveTime)
    }

    override fun track(booster: Trackable) {
        trackables.add(booster)
        if (statusMap.containsKey(booster.productId)){
            booster.duration?.let {
                val balance = if (statusMap[booster.productId] != null) statusMap[booster.productId]!! else 0
                statusMap[booster.productId] = balance + it
            }
        } else {
            statusMap[booster.productId] = booster.duration
        }
    }

    override fun activeBoosterIds(): Set<String> {
        return statusMap.keys
    }

    override fun add(listener: BoosterListener) {
        boosterListeners.add(listener)
    }

    override fun remove(listener: BoosterListener) {
        boosterListeners.remove(listener)
    }

    private fun checkTime() {
        val nextStatus: MutableMap<String, Int?> = mutableMapOf()
        statusMap.forEach{
            if (it.value == null){
                nextStatus[it.key] = it.value
            } else {
                val nextValue = it.value!! - 1
                if (nextValue > 0){
                    nextStatus[it.key] = nextValue
                    notifyTimeUpdate(it.key, nextValue)
                } else {
                    notifyTimeEnd(it.key)
                    trackables.remove(trackables.first { trackable ->
                        trackable.productId == it.key
                    })
                }
            }
        }
        statusMap.clear()
        statusMap.putAll(nextStatus)
        handler.postDelayed(trackerRunnable, checkTime)
    }

    private fun saveByDelay(){
        storage.save(statusMap)
        handler.postDelayed(saveRunnable, saveTime)
    }

    private fun notifyTimeUpdate(id: String, timeLeft: Int){
        val trackable = trackables.firstOrNull { it.productId == id }
        if (trackable != null){
            boosterListeners.forEach {
                it.onTimeUpdate(trackable, timeLeft)
            }
        }
    }

    private fun notifyTimeEnd(id: String){
        val trackable = trackables.firstOrNull { it.productId == id }
        if (trackable != null){
            boosterListeners.forEach {
                it.onTimeEnd(trackable)
            }
        }
    }

}