package hk.test.booster_module.logic

import android.content.Context
import hk.test.booster_module.Trackable
import hk.test.booster_module.Utils.toMap
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStreamWriter


class TrackerStorage(val context: Context) {

    private val fileName = "booster.data"

    fun save(data: Map<String, Int?>){
        val json = JSONObject(data)
        val outputStreamWriter = OutputStreamWriter(
            context.openFileOutput(
                fileName,
                Context.MODE_PRIVATE
            )
        )
        outputStreamWriter.write(json.toString())
        outputStreamWriter.close()
    }

    fun get(): Map<String, Int?> {
        val inputStream: InputStream? = context.openFileInput(fileName)

        if (inputStream != null) {
            val inputStreamReader = InputStreamReader(inputStream)
            val bufferedReader = BufferedReader(inputStreamReader)
            var receiveString: String? = ""
            val stringBuilder = StringBuilder()
            while (bufferedReader.readLine().also { receiveString = it } != null) {
                stringBuilder.append(receiveString)
            }
            inputStream.close()
            val ret = stringBuilder.toString()
            val json = JSONObject(ret)
            json.toMap()
        }
        return mapOf()
    }

    fun loadTrackableById(ids: Set<String>): Set<Trackable>{

        //TODO: Imp logic to getting trackable objects by ids.
        return setOf()
    }

}