package hk.test.booster_module.Utils


import org.json.JSONArray
import org.json.JSONObject


fun JSONObject.toMap(): MutableMap<String, Any> {
    val map: MutableMap<String, Any> =
        HashMap()

    val keysItr: Iterator<String> = this.keys()
    while (keysItr.hasNext()) {
        val key = keysItr.next()
        var value: Any = this.get(key)
        if (value is JSONArray) {
            value = value.toList()
        } else if (value is JSONObject) {
            value = value.toMap()
        }
        map[key] = value
    }
    return map
}

fun JSONArray.toList(): MutableList<Any> {
    val list: MutableList<Any> = ArrayList()
    for (i in 0 until this.length()) {
        var value: Any = this.get(i)
        if (value is JSONArray) {
            value = value.toList()
        } else if (value is JSONObject) {
            value = value.toMap()
        }
        list.add(value)
    }
    return list
}